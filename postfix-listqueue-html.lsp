<% local data, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<% htmlviewfunctions.displaycommandresults({"flushqueue"}, session) %>

<% local header_level = htmlviewfunctions.displaysectionstart(data, page_info) %>
<pre>
<%= html.html_escape(data.value) %>
</pre>
<% htmlviewfunctions.displayitem(cfe({type="form", value={}, label="Flush Queue", option="Flush", action="flushqueue" }), page_info, 0) %>
<% htmlviewfunctions.displaysectionend(header_level) %>
