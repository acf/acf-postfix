local mymodule = {}

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.startstop(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

function mymodule.details(self)
	return self.model.getstatusdetails()
end

function mymodule.listfiles(self)
	return self.model.getfilelist()
end

function mymodule.createfile(self)
	return self.handle_form(self, self.model.getnewfile, self.model.createfile, self.clientdata, "Create", "Create new file", "New file created")
end

function mymodule.deletefile(self)
	return self.handle_form(self, self.model.getdeletefile, self.model.deletefile, self.clientdata, "Delete", "Delete file", "File deleted")
end

function mymodule.expert(self)
	return self.handle_form(self, function() return self.model.getfiledetails(self.clientdata.filename) end, self.model.updatefiledetails, self.clientdata, "Save", "Edit Postfix File", "File Saved")
end

function mymodule.rebuilddatabases(self)
	return self.handle_form(self, self.model.get_rebuild_databases, self.model.rebuild_databases, self.clientdata, "Rebuild", "Rebuild Databases")
end

function mymodule.listqueue(self)
	return self.model.getmailqueue()
end

function mymodule.flushqueue(self)
	return self.handle_form(self, self.model.getflushqueue, self.model.flushqueue, self.clientdata, "Flush", "Flush Queue")
end

function mymodule.logfile(self)
	return self.model.get_logfile(self, self.clientdata)
end

return mymodule
