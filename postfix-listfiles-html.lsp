<% local data, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#list").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
	});
</script>

<% htmlviewfunctions.displaycommandresults({"deletefile", "expert", "rebuilddatabases"}, session) %>
<% htmlviewfunctions.displaycommandresults({"createfile"}, session, true) %>

<% if viewlibrary and viewlibrary.dispatch_component then
	viewlibrary.dispatch_component("status")
end %>

<% local header_level = htmlviewfunctions.displaysectionstart(data, page_info) %>
<table id="list" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>File</th>
		<th>Size</th>
		<th>Last Modified</th>
	</tr>
</thead><tbody>
<% local filename = cfe({ type="hidden", value="" }) %>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for i,file in ipairs(data.value) do %>
	<tr>
		<td>
		<% filename.value = file.filename %>
		<% htmlviewfunctions.displayitem(cfe({type="link", value={filename=filename, redir=redir}, label="", option="Edit", action="expert"}), page_info, -1) %>
		<% if file.filename ~= "/etc/mail/aliases" then %>
			<% htmlviewfunctions.displayitem(cfe({type="form", value={filename=filename}, label="", option="Delete", action="deletefile"}), page_info, -1) %>
		<% end %>
		</td>
		<td><%=  html.html_escape(file.filename) %></td>
		<td><span class="hide"><%= html.html_escape(file.size or 0) %>b</span><%= format.formatfilesize(file.size) %></td>
		<td><%= format.formattime(file.mtime) %></td>
	</tr>
<% end %>
</tbody></table>

<% htmlviewfunctions.displayitem(cfe({type="form", value={}, label="Rebuild Databases", option="Rebuild", action="rebuilddatabases"}), page_info, 0) %>
<% htmlviewfunctions.displaysectionend(header_level) %>

<% if viewlibrary and viewlibrary.dispatch_component and viewlibrary.check_permission("createfile") then
	local newfileform = viewlibrary.dispatch_component("createfile", nil, true) %>
<%
	newfileform.action = page_info.script .. page_info.prefix .. page_info.controller .. "/createfile"
	htmlviewfunctions.displayitem(newfileform, page_info, htmlviewfunctions.incrementheader(header_level))
end %>
